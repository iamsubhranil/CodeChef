#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

int main(){
    uint64_t testCases;
    scanf("%" SCNu64, &testCases);
    while(testCases--){
        uint64_t noe, count = 0;
        scanf("%" SCNu64, &noe);
        uint64_t arr[noe];
        for(uint64_t i = 0;i < noe;i++)
            scanf("%" SCNu64, &arr[i]);
        for(uint64_t i = 0;i < noe;i++){
            uint64_t sum = 0, product = 1;
            for(uint64_t j = i;j < noe;j++){
                sum += arr[j];
                product *= arr[j];

                if(sum == product)
                    count++;
            }
        }
        printf("%" PRIu64 "\n", count);
    }
}
