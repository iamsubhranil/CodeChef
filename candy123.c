#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>

static uint64_t findbob(uint64_t x){
    uint64_t i = 0;
    while(i <= floor(sqrt(x - i)))
        i++;
    return i - 1;
}

int main(){
    uint64_t testcases = 0;
    scanf("%" PRIu64, &testcases);
    while(testcases--){
        uint64_t limak, bob;
        scanf("%" PRIu64, &limak); scanf("%" PRIu64, &bob);
        limak = floor(sqrt(limak)); bob = findbob(bob);
        
        if(limak > bob)
            printf("Limak\n");
        else
            printf("Bob\n");
    }
}
