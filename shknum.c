#include <stdio.h>

#define size (sizeof(int) * 8)

int main(){
    unsigned long t;
    scanf("%ld", &t);
    while(t--){
        unsigned long num;
        scanf("%ld", &num);
        unsigned long count = 0, highestbit = 0;
        unsigned long bits[size] = {0}, num1 = 0;
        while(count < size){
            bits[count] = (num & (1 << count)) > 0;
            num1 += bits[count];
            if(bits[count])
                highestbit = count;
            count++;
        }
        unsigned long prev = (1 << highestbit), next = (1 << highestbit);
        int req = -1;
        switch(num1){
            case 0:
                req = 3; break;
            case 2:
                req = 0; break;
            case 1:
                {
                    if(num == 1){
                        req = 2;
                        break;
                    }
                    else{
                        prev = (1 << (highestbit - 1));
                        prev |= (1 << (highestbit - 2));
                        next |= 1;
                        break;
                    }
                }
            default:
                {
                    unsigned long shighestbit = highestbit - 1;
                    while(shighestbit > 0){
                        if(bits[shighestbit]){
                            prev |= (1 << shighestbit);
                            if(highestbit != shighestbit + 1)
                                next |= (1 << (shighestbit + 1));
                            else{
                                next = 1;
                                next |= (1 << (shighestbit + 2));
                            }
                            break;
                        }
                        shighestbit--;
                    }
                }
                break;
        }
        if(req == -1){
            //printf("\n%ld %ld\n", prev, next);

            unsigned long pdiff = num - prev, ndiff = next - num;
            if(pdiff > ndiff)
                req = ndiff;
            else
                req = pdiff;
        }
        printf("%d\n", req);
    }
}
