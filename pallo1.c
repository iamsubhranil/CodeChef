#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

int main(){
    uint64_t testcases = 0;
    scanf("%" SCNu64, &testcases);
    while(testcases--){
        uint8_t pal = 1;
        char number[5];
        scanf("%s", number);
        uint64_t i = 0, j = strlen(number) - 1;
        while(i < j){
            if(number[i] == number[j]){
                i++; j--;
            }
            else{
                pal = 0;
                break;
            }
        }
        if(pal){
            printf("wins\n");
        }
        else
            printf("losses\n");
    }
}
