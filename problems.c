#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int problemNo, n;
} Difficulty;

typedef struct{
    int sc, ns;
} Subtask;

#ifndef USE_QUICKSORT
#define USE_MERGESORT
#endif

Difficulty diffs[100000] = {{0, 0}};
int final[100000] = {0};
Subtask subtasks[30] = {{0, 0}};
#ifdef USE_MERGESORT
Subtask auxsubtask[30] = {{0, 0}};
Difficulty aux[100000] = {{0, 0}};
#endif

#ifdef USE_MERGESORT

#define is_greater(idx1, idx2)                                  \
    ((diffs[idx1].n > diffs[idx2].n)                            \
     || ((diffs[idx1].n == diffs[idx2].n)                       \
         && (diffs[idx1].problemNo > diffs[idx2].problemNo)))

static void sorted_merge(int beg, int mid, int end){
    
    int auxp = 0;
    int nbeg = beg;
    int nmid = mid + 1;
    while(nbeg <= mid && nmid <= end){
        if(is_greater(nbeg, nmid)){
            aux[auxp] = diffs[nmid];
            nmid++;
        }
        else{
            aux[auxp] = diffs[nbeg];
            nbeg++;
        }
        auxp++;
    }
    while(nbeg <= mid)
        aux[auxp++] = diffs[nbeg++];
    while(nmid <= end)
        aux[auxp++] = diffs[nmid++];
    for(int i = 0;i < auxp;i++){
        diffs[beg + i] = aux[i];
        final[aux[i].problemNo] = beg + i;
    }
}

static void merge_sort_diffs(int from, int to){
    if(from >= to)
        return;
    int mid = (from + to)/2;
    merge_sort_diffs(from, mid);
    merge_sort_diffs(mid + 1, to);
    sorted_merge(from, mid, to);
}

static void sorted_merge_ns_sc(int beg, int mid, int end){
    int auxp = 0;
    int nbeg = beg;
    int nmid = mid + 1;
    while(nbeg <= mid && nmid <= end){
        if(subtasks[nbeg].sc > subtasks[nmid].sc){
            auxsubtask[auxp] = subtasks[nmid];
            nmid++;
        }
        else{
            auxsubtask[auxp] = subtasks[nbeg];
            nbeg++;
        }
        auxp++;
    }
    while(nbeg <= mid){
        auxsubtask[auxp] = subtasks[nbeg];
        auxp++;
        nbeg++;
    }
    while(nmid <= end){
        auxsubtask[auxp] = subtasks[nmid];
        auxp++;
        nmid++;
    }
    for(int i = 0;i < auxp;i++){
        subtasks[beg + i] = auxsubtask[i];
    }
}

static void merge_sort_ns_sc(int from, int to){
    if(from >= to)
        return;
    int mid = (from + to)/2;
    merge_sort_ns_sc(from, mid);
    merge_sort_ns_sc(mid + 1, to);
    sorted_merge_ns_sc(from, mid, to);
}
#endif

#ifdef USE_QUICKSORT
int subtask_comp(const void *t1, const void *t2){
    Subtask *s1 = (Subtask *)t1;
    Subtask *s2 = (Subtask *)t2;
    return s1->sc == s2->sc ? 0 :
        s1->sc < s2->sc ? -1    :
        1;
}

int diff_comp(const void *t1, const void *t2){
    Difficulty *d1 = (Difficulty *)t1;
    Difficulty *d2 = (Difficulty *)t2;
    return d1->n > d2->n ? 1 :
            d1->n < d2->n ? -1 :
            d1->problemNo > d2->problemNo ? 1 :
            d1->problemNo < d2->problemNo ? -1 :
            0;
}
#endif

int main(){
    int P = 0, S = 0, problemCount = 0;
    scanf("%d", &P);
    scanf("%d", &S);
    while(P--){
#ifdef DEBUG
        printf("\nScanning scores for %d subtasks\n", S);
#endif
        for(int i = 0;i < S;i++)
            scanf("%d", &(subtasks[i].sc));
#ifdef DEBUG
        printf("\nScanning number of contestants solved for %d subtasks\n", S);
#endif
        for(int i = 0;i < S;i++)
            scanf("%d", &(subtasks[i].ns));
#ifdef USE_QUICKSORT
        qsort(subtasks, S, sizeof(Subtask), &subtask_comp);
#else
        merge_sort_ns_sc(0, S - 1);
#endif
#ifdef DEBUG
        for(int i = 0;i < S;i++)
            printf("\n%d %d", subtasks[i].sc, subtasks[i].ns);
#endif
        int n = 0;
        for(int i = 0;i < S - 1;i++)
            n += (subtasks[i].ns > subtasks[i + 1].ns);
        diffs[problemCount].problemNo = problemCount;
        diffs[problemCount].n = n;
        problemCount++;
    }
    //printf("\nProblems : %d", problemCount);

#ifdef USE_QUICKSORT
    qsort(diffs, problemCount, sizeof(Difficulty), &diff_comp);
#else
    merge_sort_diffs(0, problemCount - 1);
#endif

#ifdef DEBUG
    for(int i = 0;i < problemCount;i++)
        printf("\n%d %d\n", diffs[i].n, diffs[i].problemNo);
#endif
    for(int i = 0;i < problemCount;i++)
        final[diffs[i].problemNo] = i;
    for(int i = 0;i < problemCount;i++)
        printf("%d\n", final[i] + 1);
    return 0;
}
