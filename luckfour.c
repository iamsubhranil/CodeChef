#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

int main(){
    uint64_t numTest = 0;
    scanf("%" SCNu64, &numTest);
    uint64_t res[numTest], pointer = 0;
    while(numTest--){
        uint64_t number, count = 0;
        scanf("%" SCNu64, &number);
        while(number > 0){
            uint64_t digit = number % 10;
            if(digit == 4)
                count++;
            number = number / 10;
        }
        res[pointer++] = count;
    }
    for(uint64_t i = 0;i < pointer;i++)
        printf("%" PRIu64 "\n", res[i]);
    return 0;
}
