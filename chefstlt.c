#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

int main(){
    uint64_t testCases;
    scanf("%" SCNu64, &testCases);
    while(testCases--){
        char s1[100], s2[100];
        scanf("%s", s1);
        scanf("%s", s2);
        uint64_t pointer = 0, max = 0, min = 0;
        while(s1[pointer] != '\0'){
            if(s1[pointer] != s2[pointer]){
                if(s1[pointer] == '?' || s2[pointer] == '?'){
                    max++;
                }
                else{
                    max++; min++;
                }
            }
            else if(s1[pointer] == '?' && s2[pointer] == '?'){
                max++;
            }
            pointer++;
        }
        printf("%" PRIu64 " %" PRIu64 "\n", min, max);
    }
    return 0;
}
