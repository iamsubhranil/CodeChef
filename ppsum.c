#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#define SUM(x) ((x*(x+1))/2)

int main(){
    uint64_t testCases;
    scanf("%" SCNu64, &testCases);
    while(testCases--){
        uint64_t D, N, res;
        scanf("%" SCNu64, &D);
        scanf("%" SCNu64, &N);
        res = N;
        while(D--)
            res = SUM(res);
        printf("%" PRIu64 "\n", res);
    }
}
