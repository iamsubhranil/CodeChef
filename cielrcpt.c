#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#define accept(x) \
    if(rem >= x) \
        rem -=x;

int main(){
    uint64_t testCases;
    scanf("%" SCNu64, &testCases);
    while(testCases--){
        uint64_t rem, menu = 0;
        scanf("%" SCNu64, &rem);
        while(rem > 0){
            accept(2048)
            else accept(1024)
            else accept(512)
            else accept(256)
            else accept(128)
            else accept(64)
            else accept(32)
            else accept(16)
            else accept(8)
            else accept(4)
            else accept(2)
            else accept(1)
            
            menu++;
        }
        printf("%" PRIu64 "\n", menu);
    }
}
