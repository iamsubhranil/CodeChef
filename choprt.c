#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

int main(){
    uint64_t test;
    scanf("%" SCNu64, &test);
    while(test--){
        uint64_t num1, num2;
        scanf("%" SCNu64, &num1);
        scanf("%" SCNu64, &num2);
        printf("%c\n", num1 == num2 ? '=' : num1 < num2 ? '<' : '>');
    }
}
