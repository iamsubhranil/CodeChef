#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

int main(){
    uint64_t testCases;
    scanf("%" SCNu64, &testCases);
    while(testCases--){
        uint64_t num;
        scanf("%" SCNu64, &num);
        uint64_t res = sqrt(num);
        printf("%" PRIu64 "\n", res);
    }
    return 0;
}
