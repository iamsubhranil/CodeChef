#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#define SUB(val) \
    if(money >= val) \
        money -= val;

int main(){
    uint64_t  testCases;
    scanf("%" SCNu64, &testCases);
    while(testCases--){
        uint64_t money, dcount = 0;
        scanf("%" SCNu64, &money);
        while(money > 0){
            SUB(100)
            else SUB(50)
            else SUB(10)
            else SUB(5)
            else SUB(2)
            else SUB(1)
            dcount++;
        }
        printf("%" PRIu64 "\n", dcount);
    }
}
