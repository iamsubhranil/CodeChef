#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]){
    int t;
    FILE *fp = stdin;
#ifdef DEBUG
    if(argc == 2){
        t = atoi(argv[1]);
        printf("Generating %d random test inputs\n", t);
        fp = fopen("spellbob.tmp", "w");
        srand(time(NULL));
        for(int i = 0;i < t;i++){
            int a = rand() % 25;
            int b = rand() % 25;
            int c = rand() % 25;
            fprintf(fp, "%c%c%c\n", a + 'a', b + 'a', c + 'a');
            a = rand() % 25;
            b = rand() % 25;
            c = rand() % 25;
            fprintf(fp, "%c%c%c\n", a + 'a', b + 'a', c + 'a');
        }
        fclose(fp);
        fp = fopen("spellbob.tmp", "r");
    }
    else
#endif
        scanf("%d", &t);
    while(t--){
       char cards[3][3];
        cards[0][2] = cards[1][2] = cards[2][2] = '\0';
        fscanf(fp, " %c%c%c %c%c%c", &cards[0][0], &cards[1][0], &cards[2][0],
                &cards[0][1], &cards[1][1], &cards[2][1]);
        /*int lettercount[2] = {0}; // b, o
        for(int i = 0;i < 3;i++){
            int res = cards[i][0] == 'o' || cards[i][1] == 'o';
            lettercount[1] += res;
            lettercount[0] += !res && (cards[i][0] == 'b' || cards[i][1] == 'b');
        }*/
#ifdef DEBUG
       char cardsbak[3][3];
        for(int i = 0;i < 3;i++){
            for(int j = 0;j < 3;j++){
                cardsbak[i][j] = cards[i][j];
            }
        }
#endif
        // Checker
        #define ROTATE(a, b)            \
            char rtemp[2];              \
            rtemp[0] = cards[a][0];     \
            rtemp[1] = cards[a][1];     \
            cards[a][0] = cards[b][0];  \
            cards[a][1] = cards[b][1];  \
            cards[b][0] = rtemp[0];     \
            cards[b][1] = rtemp[1];

        #define FLIP(a)                 \
            char ftemp;                 \
            ftemp = cards[a][0];        \
            cards[a][0] = cards[a][1];  \
            cards[a][1] = ftemp;
        int match = 0;
        for(int i = 0;i < 3;i++){
            if(cards[i][0] == 'b' || cards[i][1] == 'b'){
                // If this contains o, try to select any other card
                if(cards[i][0] == 'o' || cards[i][1] == 'o'){
                    int temp = i + 1;
                    while(temp < 3){
                        if(cards[temp][0] == 'b' || cards[temp][1] == 'b'){
                            i = temp;
                            break;
                        }
                        temp++;
                    }
                }
#ifdef DEBUG
                if(cards[i][1] == 'b'){
                    FLIP(i);
                }
#endif
                ROTATE(i, 0);
                match = 1;
                break;
            }
        }
        if(match)
            match = 0;
        else
            goto printresult;
        for(int i = 1;i < 3;i++){
            if(cards[i][0] == 'o' || cards[i][1] == 'o'){
                // If this card contains b, try to select the other card
                if(i == 1 && (cards[1][0] == 'b' || cards[1][1] == 'b')){
                    if(cards[2][0] == 'o' || cards[2][1] == 'o')
                        i = 2;
                }
#ifdef DEBUG
                if(cards[i][1] == 'o'){
                    FLIP(i);
                }
#endif
                ROTATE(i, 1);
                match = 1;
                break;
            }
        }
        if(match)
            match = 0;
        else
            goto printresult;
        if(cards[2][0] == 'b' || cards[2][1] == 'b'){
#ifdef DEBUG
            if(cards[2][1] == 'b'){
                FLIP(2);
            }
#endif
            match = 1;
        }
printresult:
        if(match){
#ifdef DEBUG
            printf("yes\t%s %s %s -> %s %s %s\t%s\n", cardsbak[0], cardsbak[1], cardsbak[2],
                    cards[0], cards[1], cards[2], match ? "correct" : "wrong");
#else
            printf("yes\n");
#endif
        }
        else
#ifdef DEBUG
            printf("no\t%s %s %s -> %s %s %s\t%s\n", cardsbak[0], cardsbak[1], cardsbak[2], 
                    cards[0], cards[1], cards[2], !match ? "correct" : "wrong");
#else
            printf("no\n");
#endif
    }
}

