#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#define scan(x) scanf("%" PRIu64, &x)

int main(){
    uint64_t testCases;
    scan(testCases);
    while(testCases--){
        uint64_t numFiles, numIgnoredFiles, numTrackedFiles;
        scan(numFiles); scan(numIgnoredFiles); scan(numTrackedFiles);
        uint64_t ignoredFiles[numIgnoredFiles];
        uint64_t trackedFiles[numTrackedFiles];
        for(uint64_t i = 0;i < numIgnoredFiles;i++)
            scan(ignoredFiles[i]);
        for(uint64_t i = 0;i < numTrackedFiles;i++)
            scan(trackedFiles[i]);
        
        uint64_t trackIgnore = 0, untrackUnignore = 0;
        for(uint64_t i = 0;i < numIgnoredFiles;i++){
            for(uint64_t j = 0;j < numTrackedFiles;j++){
                if(ignoredFiles[i] == trackedFiles[j])
                    trackIgnore++;
            }
        }

        uint64_t allFiles = 0, i = 0, j = 0;
        while(i < numTrackedFiles && j < numIgnoredFiles){
            if(trackedFiles[i] == ignoredFiles[j]){
                i++; j++;
            }
            else if(trackedFiles[i] > ignoredFiles[j])
                j++;
            else
                i++;
            allFiles++;
        }
        allFiles += (numTrackedFiles - i) + (numIgnoredFiles - j);
        untrackUnignore = numFiles - allFiles;

        printf("%" PRIu64 " %" PRIu64 "\n", trackIgnore, untrackUnignore);
     }
}
